package payasoft.atc5;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import net.time4j.PlainDate;
import net.time4j.calendar.PersianCalendar;
import net.time4j.format.expert.ChronoFormatter;

import java.util.Locale;

public class PlugActivity extends AppCompatActivity {

    TextView chargeValue;
    TextView dateValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plug);

//        Intent intent = getIntent();
//        int level = intent.getIntExtra("BatteryLevel", -17);

        chargeValue = findViewById(R.id.chargeValue);
        dateValue = findViewById(R.id.dateValue);


        chargeValue.setText(String.format("%% %d", getBatteryPercentage(this)));
        PlainDate today = PlainDate.nowInSystemTime();
        dateValue.setText(getDateTitle(today));

    }

    public static int getBatteryPercentage(Context context) {

        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);

        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

        float batteryPct = level / (float) scale;

        return (int) (batteryPct * 100);
    }

    public String getDateTitle(PlainDate gregorianDate) {
        String res;
        ChronoFormatter f;

        PersianCalendar jalali = gregorianDate.transform(PersianCalendar.class);
        f = ChronoFormatter.setUp(PersianCalendar.axis(), Locale.getDefault())
                .addText(PersianCalendar.DAY_OF_WEEK)
                .addLiteral('،')
                .addLiteral(' ')
                .addInteger(PersianCalendar.DAY_OF_MONTH, 1, 2)
                .addLiteral(' ')
                .addText(PersianCalendar.MONTH_OF_YEAR)
                .addLiteral(' ')
                .addInteger(PersianCalendar.YEAR_OF_ERA, 1, 4)
                .build();
        res = f.format(jalali);

        return res;
    }
}
