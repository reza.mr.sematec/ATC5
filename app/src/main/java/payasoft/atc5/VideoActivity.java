package payasoft.atc5;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoActivity extends AppCompatActivity {

    VideoView mVideoView;
    BroadcastReceiver callReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        if (ContextCompat.checkSelfPermission(VideoActivity.this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(VideoActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    1500);
        }


        bindViews();
        Uri uri = Uri.parse("https://as10.cdn.asset.aparat.com/aparat-video/7e13efd1eb07ec2339cab2df5e98ace57173126-176p__20972.mp4");
        mVideoView.setMediaController(new MediaController(this));
        mVideoView.setVideoURI(uri);

        callReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(getApplication(), "Calliiiiiing", Toast.LENGTH_SHORT).show();
                if (mVideoView.isPlaying())
                    mVideoView.pause();
            }
        };

        IntentFilter callFilter = new IntentFilter("android.intent.action.PHONE_STATE");
        registerReceiver(callReceiver, callFilter);
    }

    private void bindViews() {
        mVideoView = (VideoView) findViewById(R.id.videoView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mVideoView.start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Toast.makeText(getApplication(), "Permission Request", Toast.LENGTH_SHORT).show();
    }
}
