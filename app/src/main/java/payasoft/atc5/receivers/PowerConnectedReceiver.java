package payasoft.atc5.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.widget.Toast;

import payasoft.atc5.PlugActivity;

/**
 * Created by Reza on 2017/11/09.
 */

public class PowerConnectedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        Intent intent0 = new Intent(context, PlugActivity.class);
        context.startActivity(intent0);

    }
}
