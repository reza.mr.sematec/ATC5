package payasoft.atc5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class BrowserActivity extends AppCompatActivity implements View.OnClickListener {
    EditText urlEditText;
    ImageButton goButton;
    Button showVideoButton;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        bindViews();
    }

    private void bindViews() {
        urlEditText = findViewById(R.id.urlEditText);
        goButton = (ImageButton) findViewById(R.id.goButton);
        goButton.setOnClickListener(this);
        showVideoButton = (Button) findViewById(R.id.showVideoButton);
        showVideoButton.setOnClickListener(this);
        webView = (WebView) findViewById(R.id.webView);
    }

    private String addHttpToUrl(String url) {
        if (!(url.toUpperCase().startsWith("HTTP://") || url.toUpperCase().startsWith("HTTPS://"))) {
            return "http://" + url;
        } else return url;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.goButton) {
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebViewClient(new WebViewClient());
            webView.loadUrl(addHttpToUrl(urlEditText.getText().toString()));
        }
        else if (view.getId() == R.id.showVideoButton) {
            Intent intent = new Intent(BrowserActivity.this, VideoActivity.class);
            startActivity(intent);
        }
    }

}
