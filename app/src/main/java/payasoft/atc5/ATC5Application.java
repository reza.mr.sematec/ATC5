package payasoft.atc5;

import android.app.Application;

import net.time4j.android.ApplicationStarter;

/**
 * Created by Reza on 2017/11/09.
 */

public class ATC5Application extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationStarter.initialize(this, true); // with prefetch on background thread
    }
}
